<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ej4</title>
</head>
<body>
	<?php
		$variable=24.5;
		echo "<b>Valor de la variable:</b> ". $variable;
		echo "<b> - Tipo de variable:</b> ". gettype($variable). "<br><br><br>";
		
		$variable="HOLA";
		echo "<b>Valor de la variable:</b> ". $variable;
		echo "<b> - Tipo de variable:</b> ". gettype($variable). "<br><br><br>";
		
		settype($variable, "integer");
		echo "<b>var_dump(variable):</b> ";
		var_dump($variable);
	?>
</body>
</html>